local commands = import "./commands.json";


local param_job(command) =
  {
    image: "alpine:latest",
    script: [
      "echo $RUN_COMMAND"
    ],
    when: "manual",
    variables: {
      RUN_COMMAND: command
    }
  };


{
  ["run " + command]: param_job(commands[command]) for command in std.objectFields(commands)
}